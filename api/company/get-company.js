'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const companyTable = require('../../database/companyTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);

    const company = await companyTable.getCompany(companyID);
    if(!company){
        return util.formatResponse(404, JSON.stringify({error: "Company not found"}));
    }

    company.latestSpanStart = util.getLatestSpanStart(company.firstSpanStart, company.spanDuration);

    return util.formatResponse(200, JSON.stringify(company));
}


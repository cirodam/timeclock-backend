'use strict';

const AWS = require('aws-sdk');
const companyTable = require('../../database/companyTable');
const util = require('../../util');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);

    const request = JSON.parse(event.body);

    const updated = await companyTable.updateCompany(companyID, request);
    return util.formatResponse(200, JSON.stringify(updated));
}
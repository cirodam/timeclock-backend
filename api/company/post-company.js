'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');
const util = require('../../util');
const companyTable = require('../../database/companyTable');
const profileTable = require('../../database/profileTable');
const tableTools = require('../../tableTools');
const userTable = require('../../database/userTable');

AWS.config.update({region: 'us-east-1'});

function sleep(ms){
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    })
}

module.exports.handler = async event => {

    const callerID = event.requestContext.authorizer.userID;

    let request = JSON.parse(event.body);
    const companyID = uuid.v4();

    let user = await userTable.getUser(callerID);
    const {firstName, lastName} = user;

    //Create the company
    let comp = await companyTable.addCompany(companyID, callerID, request.companyName, request.companyCode)
    if(comp.error){
        return util.formatResponse(400, JSON.stringify(comp));
    }    
    comp.latestSpanStart = util.getLatestSpanStart(comp.firstSpanStart, comp.spanDuration);

    //The user is associated with an additional company
    let newUser = await userTable.addCompanyForUser(callerID, {companyID, companyName: request.companyName});
    if(newUser.error){
        return util.formatResponse(400, JSON.stringify(newUser));
    }

    //A table is created for the company's timesheets
    let res = await tableTools.createTimesheetTable(companyID);
    if(res.error){
        return util.formatResponse(400, JSON.stringify(res));
    }

    //A table is created for the company's profiles
    res = await tableTools.createUsersTable(companyID);
    if(res.error){
        return util.formatResponse(400, JSON.stringify(res));
    }

    await sleep(6000);

    //Add an inital entry to the user's table
    res = await profileTable.addProfile(companyID, callerID, {userID: callerID, firstName, lastName, permissions: {VIEW_ADMIN: true}});
    if(res.error){
        return util.formatResponse(400, JSON.stringify(res));
    }

    return util.formatResponse(200, JSON.stringify({user: newUser, company: comp}));
};

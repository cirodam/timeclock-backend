'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');
const util = require('../../util');
const userTable = require('../../database/userTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    let request = JSON.parse(event.body);
    const userID = uuid.v4();

    if(!request.username || !request.password) {
        return util.formatResponse(400, JSON.stringify({error: "Cannot add user. Username and password are required."}));
    }

    const res = await userTable.addUser(userID, request);

    return util.formatResponse(res.error ? 400 : 200, JSON.stringify(res));
};

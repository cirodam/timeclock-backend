'use strict';

const AWS = require('aws-sdk');
const userTable = require('../../database/userTable');
const util = require('../../util');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    //const actorDepartment = event.requestContext.authorizer.departmentID;
    //const actorID = event.requestContext.authorizer.userID;

    const departmentID = decodeURIComponent(event.pathParameters.departmentID);
    const userID = decodeURIComponent(event.pathParameters.userID);

    const user = await userTable.getUser(departmentID, userID);
    if(!user){
        return util.formatResponse(401, JSON.stringify({error: "User does not exist"}));
    }

    await userTable.deleteUser(departmentID, userID);
    return util.formatResponse(200, JSON.stringify({message: "User deleted"}));
}

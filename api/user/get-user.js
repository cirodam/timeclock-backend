'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const userTable = require('../../database/userTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const userID = decodeURIComponent(event.pathParameters.userID);

    //Get the user and return if they don't exist
    let user = await userTable.getUser(userID)

    user.password = undefined;
    return util.formatResponse(200, JSON.stringify(user));
}


'use strict';

const AWS = require('aws-sdk');
const userTable = require('../../database/profileTable');
const util = require('../../util');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async event => {

    const departmentID = decodeURIComponent(event.pathParameters.departmentID);
    const userID = decodeURIComponent(event.pathParameters.userID);

    const request = JSON.parse(event.body);

    const updated = await userTable.updateUser(departmentID, userID, request);
    return util.formatResponse(200, JSON.stringify(updated));
}
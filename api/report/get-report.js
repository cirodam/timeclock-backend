'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const timesheetTable = require('../../database/timesheetTable');
const profileTable = require('../../database/profileTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);
    const startTime = +decodeURIComponent(event.queryStringParameters.startTime);

    const users = await profileTable.getProfiles(companyID);
    let report = [];

    for(let x=0; x<users.length; x++){
        let user = users[x];

        const timesheet = await timesheetTable.getTimeSheet(companyID, user.userID, startTime);
        if(!timesheet){
            report.push({
                userID: user.userID,
                firstName: user.firstName,
                lastName: user.lastName,
                days: [],
                sum: 0
            })
            continue;
        }

        const {days} = timesheet;

        report.push({
            userID: user.userID,
            firstName: user.firstName,
            lastName: user.lastName,
            days,
            sum: 0
        })
    }

    return util.formatResponse(200, JSON.stringify(report));
}

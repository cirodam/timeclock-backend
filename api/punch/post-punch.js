'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const timesheetTable = require('../../database/timesheetTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);
    const userID = decodeURIComponent(event.pathParameters.userID);
    const startTime = +decodeURIComponent(event.pathParameters.startTime);
    const timestamp = +decodeURIComponent(event.queryStringParameters.timestamp);

    const res = await timesheetTable.addPunch(companyID, userID, startTime, timestamp)

    return util.formatResponse(200, JSON.stringify(res));
};

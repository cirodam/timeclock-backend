'use strict';

const AWS = require('aws-sdk');
const timesheetTable = require('../../database/timesheetTable');
const util = require('../../util');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);
    const userID = decodeURIComponent(event.pathParameters.userID);
    const startTime = +decodeURIComponent(event.pathParameters.startTime);
    const timestamp = +decodeURIComponent(event.queryStringParameters.timestamp);

    const res = await timesheetTable.deletePunch(companyID, userID, startTime, timestamp)
    return util.formatResponse(200, JSON.stringify(res));
}

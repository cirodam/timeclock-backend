'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const timesheetTable = require('../../database/timesheetTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);
    const userID = decodeURIComponent(event.pathParameters.userID);
    const startTime = +decodeURIComponent(event.pathParameters.startTime);

    const {notes} = JSON.parse(event.body);
    const res = await timesheetTable.updateTimesheetNotes(companyID, userID, startTime, notes);

    return util.formatResponse(200, JSON.stringify(res));
}

'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const profileTable = require('../../database/profileTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);

    const users = await profileTable.getProfiles(companyID);
    if(!users){
        return util.formatResponse(404, JSON.stringify({error: "Company not found"}));
    }

    return util.formatResponse(200, JSON.stringify(users));
}

'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const profileTable = require('../../database/profileTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);
    const userID = decodeURIComponent(event.pathParameters.userID);

    const res = await profileTable.getProfile(companyID, userID);
    return util.formatResponse(res.error ? 400 : 200, JSON.stringify(res));
};

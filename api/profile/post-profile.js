'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const profileTable = require('../../database/profileTable');
const companyTable = require('../../database/companyTable');
const userTable = require('../../database/userTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);
    let request = JSON.parse(event.body);

    const company = await companyTable.getCompany(companyID);

    const dat = await userTable.addCompanyForUser(request.userID, {companyID, companyName: company.companyName})

    const res = await profileTable.addProfile(companyID, request.userID, request);
    return util.formatResponse(res.error ? 400 : 200, JSON.stringify(res));
};

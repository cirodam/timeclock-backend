'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const profileTable = require('../../database/profileTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);
    const userID = decodeURIComponent(event.pathParameters.userID);
    let request = JSON.parse(event.body);

    const res = await profileTable.updateProfile(companyID, userID, request);
    return util.formatResponse(res.error ? 400 : 200, JSON.stringify(res));
};

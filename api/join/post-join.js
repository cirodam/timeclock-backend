'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const profileTable = require('../../database/profileTable');
const companyTable = require('../../database/companyTable');
const userTable = require('../../database/userTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const callerID = event.requestContext.authorizer.userID;

    let request = JSON.parse(event.body);
    let {companyCode, inviteCode, firstName, lastName} = request;

    const company = await companyTable.getCompanyWithCode(companyCode);
    if(!company){
        return util.formatResponse(400, JSON.stringify("Cannot find company"));
    }   

    if(inviteCode !== company.inviteCode) {
        return util.formatResponse(400, JSON.stringify("Invalid invite Code"));
    }

    let res = await profileTable.addProfile(company.companyID, callerID, {firstName, lastName})
    if(res.error){
        return util.formatResponse(400, JSON.stringify(res));
    }

    res = await userTable.addCompanyForUser(callerID, {companyID: company.companyID, companyName: company.companyName})
    if(res.error){
        return util.formatResponse(400, JSON.stringify(res));
    }

    return util.formatResponse(200, JSON.stringify({company: res}));
};

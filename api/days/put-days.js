'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const timesheetTable = require('../../database/timesheetTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);
    const userID = decodeURIComponent(event.pathParameters.userID);
    const startTime = +decodeURIComponent(event.pathParameters.startTime);

    const {days} = JSON.parse(event.body);
    const res = await timesheetTable.updateTimesheetDays(companyID, userID, startTime, days);

    return util.formatResponse(200, JSON.stringify(res));
}

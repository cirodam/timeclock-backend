'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const timesheetTable = require('../../database/timesheetTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const departmentID = decodeURIComponent(event.pathParameters.departmentID);
    const userID = decodeURIComponent(event.pathParameters.userID);
    const startTime = +decodeURIComponent(event.pathParameters.startTime);

    const {punches, days, approvedBy, approvedTime, notes, startClockedIn} = JSON.parse(event.body);
    const res = await timesheetTable.updateTimesheet(departmentID, userID, startTime, punches, days, approvedBy, approvedTime, notes, startClockedIn);

    return util.formatResponse(200, JSON.stringify(res));
}

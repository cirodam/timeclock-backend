'use strict';

const AWS = require('aws-sdk');
const util = require('../../util');
const timesheetTable = require('../../database/timesheetTable');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const companyID = decodeURIComponent(event.pathParameters.companyID);
    const userID = decodeURIComponent(event.pathParameters.userID);
    const startTime = +decodeURIComponent(event.pathParameters.startTime);

    let timesheet = await timesheetTable.getTimeSheet(companyID, userID, startTime);
    if(timesheet && timesheet.error){
        return util.formatResponse(400, JSON.stringify(timesheet));
    }

    if(!timesheet){
        timesheet = await timesheetTable.createTimesheet(companyID, userID, startTime);
    }
    return util.formatResponse(200, JSON.stringify(timesheet));
}

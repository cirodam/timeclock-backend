'use strict';

const AWS = require('aws-sdk');
const jwt = require('jsonwebtoken');
const util = requre('../../util');

AWS.config.update({region: 'us-east-1'});

const userTable = require('../../database/userTable');

module.exports.handler = async event => {

    const {token, password} = JSON.parse(event.body);

    try {

        const decoded = jwt.verify(token, "SECRET");
        const {userID} = decoded;

        await userTable.updateUser(userID, {password})

        return util.formatResponse(200, "Success");
        
    } catch (err) {
        console.log(err);
    }
}
'use strict';

const AWS = require('aws-sdk');
const jwt = require('jsonwebtoken');
const userTable = require('../../database/userTable');
const util = require('../../util');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const {username, password} = JSON.parse(event.body);

    const user = await userTable.getUserWithUsername(username);
    if(!user){
        return util.formatResponse(400, JSON.stringify({error: "Incorrect Email or password"}));
    }

    //Check if the provided password matches this user's password
    if(!(await util.doPasswordsMatch(password, user.password))){
        return util.formatResponse(400, JSON.stringify({error: "Incorrect email or password"}));
    }

    //Generate jwt and return
    const {userID} = user;
    const payload = {userID};
    const token = jwt.sign(payload, "SECRET", {expiresIn: 1200});
    let expire_time = Date.now() + 1200*1000;

    return util.formatResponse(200,  JSON.stringify({token, expire_time, user}));
}

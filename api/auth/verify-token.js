'use strict';

const AWS = require('aws-sdk');
const jwt = require('jsonwebtoken');

AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const token = event.authorizationToken;
    const method = event.methodArn;

    try {

        const decoded = jwt.verify(token, "SECRET");
        return {
            principalId: "test",
            policyDocument: {
                Version: "2012-10-17",
                Statement: [
                    {
                        Action: "execute-api:Invoke",
                        Effect: "Allow",
                        Resource: method
                    }
                ]
            },
            context: {
                userID: decoded.userID
            }
        }
        
    } catch (err) {
        console.log(err);
        throw new Error("Unauthorized");
    }
}
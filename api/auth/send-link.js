'use strict';

const AWS = require('aws-sdk');
const jwt = require('jsonwebtoken');
const util = require('../../util');
const userTable = require('../../database/userTable');

const ses = new AWS.SES();
AWS.config.update({region: 'us-east-1'});

module.exports.handler = async event => {

    const {username} = JSON.parse(event.body);

    const user = await userTable.getUserWithUsername(username);
    if(!user){
        return util.formatResponse(400, JSON.stringify({error: "Username not recognized"}));
    }

    //Generate jwt and return
    const {userID, email} = user;
    const payload = {userID};
    const token = jwt.sign(payload, "SECRET", {expiresIn: 1200});

    const params = {
        Message: {
            Body: {
                Text: {
                    Charset: "UTF-8",
                    Data: `Reset Password: localhost:3000/resetpassword?token=${token}`
                }
            },
            Subject: {
                Charset: "UTF-8",
                Data: "Password Reset Request"
            }
        },
        Destination: {
            ToAddresses: [email]
        },
        Source: "arctetrasoftware@gmail.com"
    }

    const res = await ses.sendEmail(params).promise();

    return util.formatResponse(200, JSON.stringify(res));
}

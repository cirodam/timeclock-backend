const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();
const constants = require("../constants");

/**
 * Adds a record to the companyTable
 *
 * @param {String} companyID The Unique ID to be assigned to this company
 * @param {String} ownerID The Unique ID for the user that owns this company.
 * @param {String} companyName 
 * @param {String} companyCode The unique ID for this company. Used when a user wants to join a company
 * @return {Object} The newly added company
 */
const addCompany = async (companyID, ownerID, companyName, companyCode) => {

    const item = {
        companyID,
        ownerID,
        companyName,
        companyCode,
        firstSpanStart: constants.defaultFirstSpanStart,
        spanDuration: constants.defaultSpanDuration,
        usersCanEdit: true,
        shiftStart: constants.defaultShiftStart,
        shiftDuration: constants.defaultShiftDuration,
        holidays: [],
        inviteCode: 123456,
        groups: ['Default'],
        requests: []
    }

    const params = {
        TableName: 'timeclock-companyTable',
        Item: item
    }

    try {
        await dynamoDB.put(params).promise();
        return item;

    } catch (error) {
        return {error};
    }
}

/**
 * Delete a record to the companyTable
 *
 * @param {String} companyID The Unique ID of the company to be removed.

 * @return {Boolean} true if successful
 */
const deleteCompany = async companyID => {

    const params = {
        TableName: 'timeclock-companyTable',
        Key: {
            "companyID": companyID
        }
    }

    try {
        await dynamoDB.delete(params).promise();
        return true;
        
    } catch (err) {
        return {err};
    }
}

/**
 * Get a record from the companyTable
 *
 * @param {String} companyID The Unique ID of the company to fetch
 * @return {Object} The requested company or null if the company doesn't exist
 */
const getCompany = async companyID => {

    const params = {
        TableName: 'timeclock-companyTable',
        KeyConditionExpression: "#d=:d",
        ExpressionAttributeValues: {
            ':d' : companyID
        },
        ExpressionAttributeNames: {
            '#d' : 'companyID'
        }
    }

    try {
        const res = await dynamoDB.query(params).promise();
        if(res.Count > 0){
            return res.Items[0];
        }
        return null;
    } catch (err) {
        return {err};
    }
}

/**
 * Get a record from the companyTable using its companyCode
 *
 * @param {String} companyCode The code for the company
 * @return {Object} The requested company or null if the company doesn't exist
 */
const getCompanyWithCode = async companyCode => {

    const params = {
        TableName: `timeclock-companyTable`,
        IndexName: 'codeIndex',
        KeyConditionExpression: '#c = :c',
        ExpressionAttributeValues: {
            ':c' : companyCode
        },
        ExpressionAttributeNames: {
            '#c' : 'companyCode'
        }
    }

    try {
        const res = await dynamoDB.query(params).promise();
        return res.Items[0];

    } catch (err) {
        return {err};
    }
}

/**
 * Update a record in the companyTable
 *
 * @param {String} companyID The Unique ID of the company to update
 * @param {Object} updates The company fields to update
 * @return {Object} The new fields for the company
 */
const updateCompany = async (companyID, updates) => {

    const {departmentName, spanDuration, firstSpanStart, usersCanEdit, shiftStart, shiftDuration, holidays, minOvertimeHours} = updates;

    let statements = [];
    const params = {
        TableName: 'timeclock-companyTable',
        Key: {
            "companyID": companyID
        },
        UpdateExpression: 'set ',
        ExpressionAttributeValues: {

        },
        ReturnValues: 'ALL_NEW'
    }

    if(departmentName) {
        statements.push('departmentName = :d');
        params.ExpressionAttributeValues[':d'] = departmentName;
    }

    if(spanDuration) {
        statements.push('spanDuration = :o');
        params.ExpressionAttributeValues[':o'] = spanDuration;
    }

    if(firstSpanStart){
        statements.push('firstSpanStart = :c');
        params.ExpressionAttributeValues[':c'] = firstSpanStart;
    }

    if(usersCanEdit !== null){
        statements.push('usersCanEdit = :u');
        params.ExpressionAttributeValues[':u'] = usersCanEdit;
    }

    if(shiftStart){
        statements.push('shiftStart = :s');
        params.ExpressionAttributeValues[':s'] = shiftStart;
    }

    if(shiftDuration){
        statements.push('shiftDuration = :t');
        params.ExpressionAttributeValues[':t'] = shiftDuration;
    }

    if(holidays){
        statements.push('holidays = :h');
        params.ExpressionAttributeValues[':h'] = holidays;
    }

    if(minOvertimeHours){
        statements.push('minOvertimeHours = :m');
        params.ExpressionAttributeValues[':m'] = minOvertimeHours;
    }

    statements.forEach((statement, index) => {
        if(index === 0){
            params.UpdateExpression += statement;

        }else{
            params.UpdateExpression += ', ' + statement;
        }
    });

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes;

    } catch (err) {
        return err;
    }
}

/**
 * Update the list of holidays for a company
 *
 * @param {String} companyID The Unique ID of the company to update
 * @param {Array} holidays The updated list of holidays.
 * @return {Array} The new list of holidays for the company
 */
const updateCompanyHolidays = async (companyID, holidays) => {

    const params = {
        TableName: 'timeclock-companyTable',
        Key: {
            "companyID": companyID,
        },
        UpdateExpression: 'set #h = :h',
        ExpressionAttributeValues: {
            ':h': holidays
        },
        ExpressionAttributeNames: {
            '#h': "holidays"
        },
        ReturnValues: 'ALL_NEW'
    }

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes.holidays;

    } catch (err) {
        return {err};

    }
}

/**
 * Update the list of groups for a company
 *
 * @param {String} companyID The Unique ID of the company to update
 * @param {Array} groups The updated list of groups for this company
 * @return {Array} The new list of groups
 */
const updateCompanyGroups = async (companyID, groups) => {

    const params = {
        TableName: 'timeclock-companyTable',
        Key: {
            "companyID": companyID,
        },
        UpdateExpression: 'set #h = :h',
        ExpressionAttributeValues: {
            ':h': groups
        },
        ExpressionAttributeNames: {
            '#h': "groups"
        },
        ReturnValues: 'ALL_NEW'
    }

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes.groups;

    } catch (err) {
        return {err};

    }
}

/**
 * Adds an Entry to this company's list of requests to join
 *
 * @param {String} companyID The Unique ID of the company receiving the request
 * @param {String} userID The Unique ID of the user that requested to join the company
 * @return {Array} The updated list of requests for the company
 */
const addRequest = async (companyID, userID) => {
    let company = await getCompany(companyID);
    let {requests} = company;

    requests.push(userID);

    const res = await updateCompanyRequests(companyID, requests);
    return res;
}

/**
 * Updates a company's list of requests to join
 *
 * @param {String} companyID The Unique ID of the company to be updated
 * @param {Array} requests The updated list of requests for the company
 * @return {Array} The updated list of requests for the company
 */
const updateCompanyRequests = async (companyID, requests) => {

    const params = {
        TableName: 'timeclock-companyTable',
        Key: {
            "companyID": companyID,
        },
        UpdateExpression: 'set #h = :h',
        ExpressionAttributeValues: {
            ':h': requests
        },
        ExpressionAttributeNames: {
            '#h': "requests"
        },
        ReturnValues: 'ALL_NEW'
    }

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes.requests;

    } catch (err) {
        return {err};

    }
}

module.exports = {
    addCompany, 
    deleteCompany, 
    getCompany, 
    getCompanyWithCode,
    updateCompany,
    updateCompanyHolidays,
    updateCompanyGroups,
    addRequest
}
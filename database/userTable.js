const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();
const util = require('../util');

/**
 * Add a new user to the userTable
 *
 * @param {String} userID The Unique ID of the user being added
 * @param {Number} userFields The fields of the user being added
 * @return {Object} The newly created user;
 */
const addUser = async (userID, userFields) => {
    const {username, password, firstName, lastName, email} = userFields;

    let item = {
        userID,
        username,
        firstName,
        lastName,
        password: await util.encrpytPassword(password),
        email,
        companies: []
    }

    const params = {
        TableName: `timeclock-userTable`,
        Item: item
    }

    try {
        await dynamoDB.put(params).promise();
        item.password = undefined;
        return item;

    } catch (err) {
        if(err.code === "ResourceNotFoundException"){
            return {error: "Cannot add user. Invalid department."};
        }
        return {err};
    }
}

/**
 * Returns a user from the userTable
 *
 * @param {String} userID The Unique ID of the user to fetch
 * @return {Object} The returned user; null if they dont exist
 */
const getUser = async userID => {
    
    const params = {
        TableName: `timeclock-userTable`,
        KeyConditionExpression: "#u=:u",
        ExpressionAttributeValues: {
            ':u' : userID
        },
        ExpressionAttributeNames: {
            '#u' : 'userID'
        }
    }

    try {
        let res = await dynamoDB.query(params).promise();
        let user = res.Items[0];
        user.password = undefined;
        return user;

    } catch (error) {
        return {error};
    }
}

/**
 * Returns a user from the userTable using only their username
 *
 * @param {String} username The Unique username for the user to return
 * @return {Object} The returned user; null if they don't exist
 */
const getUserWithUsername = async username => {

    const params = {
        TableName: `timeclock-userTable`,
        IndexName: 'usernameIndex',
        KeyConditionExpression: '#u = :u',
        ExpressionAttributeValues: {
            ':u' : username
        },
        ExpressionAttributeNames: {
            '#u' : 'username'
        }
    }

    try {
        const res = await dynamoDB.query(params).promise();
        return res.Items[0];

    } catch (err) {
        return {err};
    }
}

/**
 * Update a user entry in the userTable
 *
 * @param {String} userID The Unique ID of the user being updated
 * @param {Object} userFields The fields of the user being updated
 * @return {Object} The newly updated user;
 */
const updateUser = async (userID, userFields) => {

    const {firstName, lastName, email, password} = userFields;

    let statements = [];
    const params = {
        TableName: `timeclock-userTable`,
        Key: {
            "userID": userID
        },
        UpdateExpression: 'set ',
        ExpressionAttributeValues: {

        },
        ReturnValues: 'ALL_NEW'
    }

    if(firstName) {
        statements.push('firstName = :e');
        params.ExpressionAttributeValues[':e'] = firstName;
    }
    
    if(lastName) { 
        statements.push('lastName = :a');
        params.ExpressionAttributeValues[':a'] = lastName;
    }

    if(email) { 
        statements.push('email = :c');
        params.ExpressionAttributeValues[':c'] = email;
    }

    if(password) { 
        let encrypted = await util.encrpytPassword(password)
        statements.push('password = :x');
        params.ExpressionAttributeValues[':x'] = encrypted;
    }

    statements.forEach((statement, index) => {
        if(index === 0){
            params.UpdateExpression += statement;

        }else{
            params.UpdateExpression += ', ' + statement;
        }
    });

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes;

    } catch (error) {
        return error;
    }
}

/**
 * Add an entry to the list of companies that a user is associated with.
 *
 * @param {String} userID The Unique ID of the user being updated
 * @param {Object} companyFields The fields for the company being added
 * @return {Array} The newly updated list of companies for this user
 */
const addCompanyForUser = async (userID, companyFields) => {

    const {companyID, companyName} = companyFields;
    const user = await getUser(userID);
    if(user.error){
        return user;
    }

    let companies = user.companies;
    companies.push({companyID, companyName});

    return await updateUserCompanies(userID, companies);
}

/**
 * Update a user's list of associated companies
 *
 * @param {String} userID The Unique ID of the user being updated
 * @param {Number} companies The updated list of companies for this user
 * @return {Array} The newly updated list of companies for this user
 */
const updateUserCompanies = async (userID, companies) => {

    const params = {
        TableName: `timeclock-userTable`,
        Key: {
            "userID": userID,
        },
        UpdateExpression: 'set #d = :d',
        ExpressionAttributeValues: {
            ':d': companies
        },
        ExpressionAttributeNames: {
            '#d': "companies"
        },
        ReturnValues: 'ALL_NEW'
    }

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes.companies;

    } catch (error) {
        return {error};

    }
}

module.exports = {
    addUser,
    getUser,
    getUserWithUsername,
    addCompanyForUser,
    updateUser
}
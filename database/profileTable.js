const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();
const util = require('../util');

/**
 * Adds an entry to this company's profileTable
 *
 * @param {String} companyID The Unique ID of the company adding the profile
 * @param {String} userID The Unique ID of the user that is having a profile added
 * @param {Object} userFields The fields to assigned to the new profile
 * @return {Object} The newly added profile
 */
const addProfile = async (companyID, userID, userFields) => {

    const {firstName, lastName, maxHolidayHours, maxOvertimeHours, group, permissions: perm} = userFields;

    let item = {
        userID,
        firstName,
        lastName,
        maxHolidayHours: maxHolidayHours ? maxHolidayHours : 0,
        maxOvertimeHours: maxOvertimeHours ? maxOvertimeHours : 0,
        group: group ? group : "Default",
        permissions: {
            VIEW_COMPANY: 0,
            EDIT_COMPANY: 0,
            VIEW_PROFILE: 0,
            EDIT_PROFILE: 0,
            VIEW_TIMESHEET: 1,
            EDIT_TIMESHEET: 1,
            APPROVE_TIMESHEET: 0
        },
    }

    for(const c in perm){
        item.permissions[c] = perm[c]
    }

    const params = {
        TableName: `profiles-${companyID}`,
        Item: item
    }

    try {
        await dynamoDB.put(params).promise();
        return item;

    } catch (error) {
        if(error.code === "ResourceNotFoundException"){
            return {error: "Cannot add user. Invalid department."};
        }
        return {error};
    }
}

/**
 * Updates an entry in a company's profileTable
 *
 * @param {String} companyID The Unique ID of the company updating the profile
 * @param {String} userID The Unique ID of the user that is having a profile updated
 * @param {Object} profileFields The fields to be updated for the profile
 * @return {Object} The newly updated profile
 */
const updateProfile = async (companyID, userID, profileFields) => {

    const {permissions, group, maxHolidayHours, maxOvertimeHours} = profileFields;

    let statements = [];
    const params = {
        TableName: `profiles-${companyID}`,
        Key: {
            "userID": userID
        },
        UpdateExpression: 'set ',
        ExpressionAttributeNames: {

        },
        ExpressionAttributeValues: {

        },
        ReturnValues: 'ALL_NEW'
    }

    if(permissions) {
        statements.push('#e = :e');
        params.ExpressionAttributeNames['#e'] = 'permissions';
        params.ExpressionAttributeValues[':e'] = permissions;
    }
    
    if(group) { 
        statements.push('#a = :a');
        params.ExpressionAttributeNames['#a'] = 'group';
        params.ExpressionAttributeValues[':a'] = group;
    }

    if(maxOvertimeHours) { 
        statements.push('#c = :c');
        params.ExpressionAttributeNames['#c'] = 'maxOvertimeHours';
        params.ExpressionAttributeValues[':c'] = maxOvertimeHours;
    }

    if(maxHolidayHours) { 
        statements.push('#x = :x');
        params.ExpressionAttributeNames['#x'] = 'maxHolidayHours';
        params.ExpressionAttributeValues[':x'] = maxHolidayHours;
    }

    statements.forEach((statement, index) => {
        if(index === 0){
            params.UpdateExpression += statement;

        }else{
            params.UpdateExpression += ', ' + statement;
        }
    });

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes;

    } catch (error) {
        return error;
    }
}

/**
 * Strips the password from an object containing all user fields
 *
 * @param {Object} user The user object being updated
 * @return {Object} The uer object with the password removed
 */
const stripPassword = user => {
    const {companyID, userID, username, firstName, lastName, email, admin, mustChangePassword, canReceiveHolidayPay, maxHolidayPay} = user;
    return {companyID, userID, username, firstName, lastName, email, admin, mustChangePassword, canReceiveHolidayPay, maxHolidayPay};
}

/**
 * Delete an entry from this company's profileTable
 *
 * @param {String} companyID The Unique ID of the company removing
 * @param {String} userID The Unique ID of the user that is having a profile removed
 * @return {Boolean} True if successful; false otherwise
 */
const deleteProfile = async (companyID, userID) => {

    const params = {
        TableName: `profiles-${companyID}`,
        Key: {
            "companyID": companyID,
            "userID": userID
        }
    }

    try {
        await dynamoDB.delete(params).promise();
        return true;
        
    } catch (err) {
        return false;
    }
}

/**
 * Gets a specific profile from the profileTable
 *
 * @param {String} companyID The Unique ID of the company the profile belongs to
 * @param {String} userID The Unique ID of the user that the profile belongs to
 * @return {Object} The requested profile. Null if the profile doesnt exist
 */
const getProfile = async (companyID, userID) => {
    
    const params = {
        TableName: `profiles-${companyID}`,
        KeyConditionExpression: "#n=:n",
        ExpressionAttributeValues: {
            ':n' : userID,
        },
        ExpressionAttributeNames: {
            '#n' : 'userID',
        }
    }

    try {
        const res = await dynamoDB.query(params).promise();
        if(res.Count > 0){
            return res.Items[0];
        }
        return null;
    } catch (err) {
        return {err};
    }
}

/**
 * Returns all profiles from a company's profileTable
 *
 * @param {String} companyID The Unique ID of the company that owns the profiles
 * @return {Array} All profiles in the profileTable
 */
const getProfiles = async (companyID) => {
    const params = {
        TableName: `profiles-${companyID}`
    }

    try {
        const res = await dynamoDB.scan(params).promise();
        return res.Items;
    } catch (err) {
        return {err};
    }
}

module.exports = {
    addProfile, 
    getProfile, 
    getProfiles, 
    deleteProfile, 
    updateProfile, 
    stripPassword
};
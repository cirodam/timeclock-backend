const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();
const companyTable = require('./companyTable');
const util = require('../util');

/**
 * Adds an entry to this company's timesheetTable
 *
 * @param {String} companyID The Unique ID of the company adding the timesheet
 * @param {String} userID The Unique ID of the user that is having a timesheet added
 * @param {Number} startTime The unique timestamp of the new timesheet
 * @return {Object} The newly added timesheet
 */
const createTimesheet = async (companyID, userID, startTime) => {

    const {spanDuration} = await companyTable.getCompany(companyID);
    const startClockedIn = await shouldStartClockedIn(companyID, userID, startTime, spanDuration);

    const item = {
        userID,
        startTime,
        punches: [],
        days: util.getDays(startTime, spanDuration),
        approvedBy: "",
        approvedTime: 0,
        notes: "",
        startClockedIn
    }

    const params = {
        TableName: `timesheets-${companyID}`,
        Item: item
    }

    try {
        await dynamoDB.put(params).promise();
        return item;

    } catch (error) {
        return {error};

    }
}

/**
 * Checks the previous timesheet for user to determine if this timesheet should start clocked in
 *
 * @param {String} companyID The Unique ID of the company
 * @param {String} userID The Unique ID of the user owning the timesheet
 * @param {Number} startTime The unique timestamp of the timesheet being checked
 * @param {Number} spanDuration The duration of this timesheet period in seconds
 * @return {Object} True if the timesheet should start clocked in
 */
const shouldStartClockedIn = async (companyID, userID, startTime, spanDuration) => {
    let startClockedIn = false;
    const last = await getTimeSheet(companyID, userID, startTime-spanDuration)
    if(last){
        startClockedIn = util.getClockedIn(last.punches, last.startClockedIn)
    }
    return startClockedIn;
}

/**
 * Get an entry from this company's timesheet table
 *
 * @param {String} companyID The Unique ID of the company
 * @param {String} userID The Unique ID of the user owning the timesheet
 * @param {Number} startTime The unique timestamp of the timesheet being returned
 * @return {Object} The returned timesheet; null if it doesn't exist
 */
const getTimeSheet = async (companyID, userID, startTime) => {

    const params = {
        TableName: `timesheets-${companyID}`,
        KeyConditionExpression: "#s=:s and #n=:n",
        ExpressionAttributeValues: {
            ':n' : userID,
            ':s' : startTime
        },
        ExpressionAttributeNames: {
            '#n' : 'userID',
            '#s' : 'startTime'
        }
    }

    try {
        let res = await dynamoDB.query(params).promise();
        let current = res.Items[0];
        if(!current){
            return null;
        }

        if(util.getClockedIn(current.punches, current.startClockedIn)){
            const company = await companyTable.getCompany(companyID)
            let next = await getTimeSheet(companyID, userID, startTime+company.spanDuration)
            if(next){
                current.punches.push(next.punches[0]);
            }
        }

        return current

    } catch (error) {
        return {error};
    }
}

/**
 * Update a timesheet in a company's timesheet table
 *
 * @param {String} companyID The Unique ID of the company
 * @param {String} userID The Unique ID of the user owning the timesheet
 * @param {Number} startTime The unique timestamp of the timesheet being updated
 * @return {Object} The newly updated timesheet
 */
const updateTimesheet = async (companyID, userID, startTime, punches, days, approvedBy, approvedTime, notes, startClockedIn) => {

    let statements = [];
    const params = {
        TableName: `timesheets-${companyID}`,
        Key: {
            "userID": userID,
            "starTime": startTime
        },
        UpdateExpression: 'set ',
        ExpressionAttributeValues: {

        },
        ReturnValues: 'ALL_NEW'
    }

    if(punches) {
        statements.push('punches = :p');
        params.ExpressionAttributeValues[':p'] = punches;
    }

    if(days) {
        statements.push('days = :d');
        params.ExpressionAttributeValues[':d'] = days;
    }

    if(approvedBy) {
        statements.push('approvedBy = :b');
        params.ExpressionAttributeValues[':b'] = approvedBy;
    }

    if(approvedTime) {
        statements.push('approvedTime = :t');
        params.ExpressionAttributeValues[':t'] = approvedTime;
    }

    if(notes) {
        statements.push('notes = :n');
        params.ExpressionAttributeValues[':n'] = notes;
    }

    if(startClockedIn) {
        statements.push('startClockedIn = :s');
        params.ExpressionAttributeValues[':s'] = startClockedIn;
    }

    statements.forEach((statement, index) => {
        if(index === 0){
            params.UpdateExpression += statement;

        }else{
            params.UpdateExpression += ', ' + statement;
        }
    });

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes;

    } catch (err) {
        return {err};
    }
}

/**
 * Add a punch to the punches list in an entry in this company's timesheet table
 *
 * @param {String} companyID The Unique ID of the company
 * @param {String} userID The Unique ID of the user owning the timesheet
 * @param {Number} startTime The unique timestamp of the timesheet being updated
 * @param {Number} timestamp The timestamp of the punch to be added
 * @return {Object} The updated list of punches for this timesheet
 */
const addPunch = async (companyID, userID, startTime, timestamp) => {

    const timesheet = await getTimeSheet(companyID, userID, startTime);
    const {punches} = timesheet;

    let index = punches.length;
    for(let i=0; i<punches.length; i++){
        if(punches[i] >= timestamp){
            index = i;
            break;
        }
    }
    punches.splice(index, 0, timestamp)

    return await updateTimesheetPunches(companyID, userID, startTime, punches);
}

/**
 * Delete a punch from the punches list in an entry in this company's timesheet table
 *
 * @param {String} companyID The Unique ID of the company
 * @param {String} userID The Unique ID of the user owning the timesheet
 * @param {Number} startTime The unique timestamp of the timesheet being updated
 * @param {Number} timestamp The timestamp of the punch to be removed
 * @return {Object} The updated list of punches for this timesheet
 */
const deletePunch = async (companyID, userID, startTime, timestamp) => {

    const timesheet = await getTimeSheet(companyID, userID, startTime);
    const {punches} = timesheet;

    //Add the punch in the array
    const sorted = punches.filter(punch => punch !== timestamp);

    return await updateTimesheetPunches(companyID, userID, startTime, sorted);
}

/**
 * Move a punch in the punches list in an entry in this company's timesheet table
 *
 * @param {String} companyID The Unique ID of the company
 * @param {String} userID The Unique ID of the user owning the timesheet
 * @param {Number} startTime The unique timestamp of the timesheet being updated
 * @param {Number} oldTimestamp The old timestamp of the punch being changed
 * @param {Number} newTimestamp The new timestamp of the punch being changed
 * @return {Object} The updated list of punches for this timesheet
 */
const movePunch = async (companyID, userID, startTime, oldTimestamp, newTimestamp) => {

    const timesheet = await getTimeSheet(companyID, userID, startTime);
    const {punches} = timesheet;
    const sorted = punches.filter(punch => punch !== oldTimestamp);

    let index = sorted.length;
    for(let i=0; i<sorted.length; i++){
        if(sorted[i] >= newTimestamp){
            index = i;
            break;
        }
    }
    sorted.splice(index, 0, newTimestamp)

    return await updateTimesheetPunches(companyID, userID, startTime, sorted);
}

/**
 * Update the entire punches list in an entry in this company's timesheet table
 *
 * @param {String} companyID The Unique ID of the company
 * @param {String} userID The Unique ID of the user owning the timesheet
 * @param {Number} startTime The unique timestamp of the timesheet being updated
 * @param {Array} punches The new list of punches for this timesheet
 * @return {Object} The updated list of punches for this timesheet
 */
const updateTimesheetPunches = async (companyID, userID, startTime, punches) => {

    const params = {
        TableName: `timesheets-${companyID}`,
        Key: {
            "userID": userID,
            "startTime": startTime
        },
        UpdateExpression: 'set #p = :p',
        ExpressionAttributeValues: {
            ':p': punches
        },
        ExpressionAttributeNames: {
            '#p': "punches"
        },
        ReturnValues: 'ALL_NEW'
    }

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes.punches;

    } catch (err) {
        return {err};

    }
}

/**
 * Update the days list in an entry in this company's timesheet table
 *
 * @param {String} companyID The Unique ID of the company
 * @param {String} userID The Unique ID of the user owning the timesheet
 * @param {Number} startTime The unique timestamp of the timesheet being updated
 * @param {Array} days The new list of days for this timesheet
 * @return {Object} The updated list of days for this timesheet
 */
const updateTimesheetDays = async (companyID, userID, startTime, days) => {

    const params = {
        TableName: `timesheets-${companyID}`,
        Key: {
            "userID": userID,
            "startTime": startTime
        },
        UpdateExpression: 'set #d = :d',
        ExpressionAttributeValues: {
            ':d': days
        },
        ExpressionAttributeNames: {
            '#d': "days"
        },
        ReturnValues: 'ALL_NEW'
    }

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes.days;

    } catch (err) {
        return {err};

    }
}

/**
 * Update the Notes field in an entry in this company's timesheet table
 *
 * @param {String} companyID The Unique ID of the company
 * @param {String} userID The Unique ID of the user owning the timesheet
 * @param {Number} startTime The unique timestamp of the timesheet being updated
 * @param {String} notes The new notes string for this timesheet
 * @return {String} The updated notes string for this timesheet
 */
const updateTimesheetNotes = async (companyID, userID, startTime, notes) => {

    const params = {
        TableName: `timesheets-${companyID}`,
        Key: {
            "userID": userID,
            "startTime": startTime
        },
        UpdateExpression: 'set #n = :n',
        ExpressionAttributeValues: {
            ':n': notes
        },
        ExpressionAttributeNames: {
            '#n': "notes"
        },
        ReturnValues: 'ALL_NEW'
    }

    try {
        const res = await dynamoDB.update(params).promise();
        return res.Attributes.notes;

    } catch (err) {
        return {err};

    }
}

module.exports = {
    createTimesheet,
    getTimeSheet,
    updateTimesheet,
    updateTimesheetPunches,
    addPunch, 
    deletePunch,
    movePunch,
    updateTimesheetDays,
    updateTimesheetNotes
}
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB;

/**
 * Creates a dynamoDB table to handle timesheets for a company
 *
 * @param {String} companyID The Unique ID of the company needing the table
 */
const createTimesheetTable = async (companyID) => {

    const params = {
        AttributeDefinitions: [
            {
                AttributeName: "userID",
                AttributeType: "S"
            },
            {
                AttributeName: "startTime",
                AttributeType: "N"
            }
        ],
        KeySchema: [
            {
                AttributeName: "userID",
                KeyType: "HASH"
            },
            {
                AttributeName: "startTime",
                KeyType: "RANGE"
            }
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 1,
            WriteCapacityUnits: 1
        },
        TableName: `timesheets-${companyID}`
    }

    try {
        const res = await dynamoDB.createTable(params).promise();
        return res;

    } catch (error) {
        return {error}
    }
}

/**
 * Creates a dynamoDB table to handle profiles for a company
 *
 * @param {String} companyID The Unique ID of the company needing the table
 */
const createUsersTable = async (companyID) => {

    const params = {
        AttributeDefinitions: [
            {
                AttributeName: "userID",
                AttributeType: "S"
            }
        ],
        KeySchema: [
            {
                AttributeName: "userID",
                KeyType: "HASH"
            }
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 1,
            WriteCapacityUnits: 1
        },
        TableName: `profiles-${companyID}`
    }

    try {
        const res = await dynamoDB.createTable(params).promise();
        return res;

    } catch (error) {
        return {error}
    }
}

/**
 * Checks whether a DynamoDB table exists
 *
 * @param {String} name The name of the table to check for
 * @return {Boolean} true if the table exists; false otherwise
 */
const doesTableExist = async name => {

    const params = {
        TableName: name
    }

    try {
        const data = await dynamoDB.describeTable(params).promise();
        console.log(data);
        return true;
    } catch (err) {
        console.log(err);
        return false;
    }
}

module.exports = {
    doesTableExist,
    createTimesheetTable,
    createUsersTable
}
const bcrypt = require('bcryptjs');

//Encrypt the provided password and return
const encrpytPassword = async password => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
}

const doPasswordsMatch = async (password, encrypted) => {
    return await bcrypt.compare(password, encrypted);
}

//Format the response to an HTTP method
const formatResponse = (statusCode, body) => {
    return {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body
    };
}

const getLatestSpanStart = (firstSpanStart, duration) => {
    const current = Date.now();
    let latest = firstSpanStart;
    while(latest + duration < current){
        latest += duration;
    }
    return latest
}

const getDays = (spanStart, duration) => {

    let durationDays = duration/1000/60/60/24;
    const start = new Date(spanStart);
    const days = {};

    for(let i=0; i<durationDays; i++){
        let next = new Date();
        next.setMonth(start.getMonth());
        next.setDate(start.getDate() + i);

        days[next.getDate()] = {
            regular: 0,
            overtime: 0,
            holiday: 0,
            vacation: 0
        }
    }

    return days;
}

const getClockedIn = (punches, startClockedIn) => {

    if(punches.length % 2 === 0){
        return startClockedIn
    }

    return !startClockedIn;
}

module.exports = {encrpytPassword, doPasswordsMatch, formatResponse, getLatestSpanStart, getDays, getClockedIn};
//Company
export const defaultFirstSpanStart = 0;
export const defaultSpanDuration = 1209600000; //Seconds
export const defaultShiftStart = 32400000;
export const defaultShiftDuration = 28800000;